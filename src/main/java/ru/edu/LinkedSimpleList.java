package ru.edu;

/**
 * Cтруктуры данных "список" на основе двусвязного списка.
 * @param <T>
 */
public class LinkedSimpleList<T> implements SimpleList<T> {

    /**
     * Variable for storing link to head element.
     */
    private Node<T> head;

    /**
     * Variable for storing link to tail element.
     */
    private Node<T> tail;

    /**
     * Size of the list.
     */
    private int size;

    /**
     * Class for storing elements of the collection.
     * @param <T>
     */
    private static class Node<T> {
        /**
         * Variable for storing link to previous element.
         */
        private Node prev;
        /**
         * Element value.
         */
        private T value;
        /**
         * Variable for storing link to next element.
         */
        private Node next;

        Node(final T val) {
            this.value = val;
        }
    }

    /**
     * Добавление элемента в конец списка.
     *
     * @param value элемент
     */
    @Override
    public void add(final T value) {

        Node<T> newNode = new Node<>(value);
        Node<T> previousTail = tail;

        if (head == null) {
                head = newNode;
        } else {
            newNode.prev = previousTail;
            previousTail.next = newNode;
        }
        tail = newNode;

        size++;

    }

    /**
     * Установка значения элемента по индексу.
     *
     * @param index индекс
     * @param value элемент
     */
    @Override
    public void set(final int index, final T value) {
        findNode(index).value = value;
    }

    /**
     * Получение элемента из списка.
     *
     * @param index индекс
     * @return значение элемента или null
     */
    @Override
    public T get(final int index) {

        Node<T> node = findNode(index);
        return node.value;

    }

    /**
     * Метод для нахождения элемента в списке по индексу.
     * @param index - индекс элемента для поиска
     * @return Node<T> - элемент списка
     */
    private Node<T> findNode(final int index) {

        if (size == 0) {
            throw new IndexOutOfBoundsException("List is empty.");
        }

        if (index < 0) {
            throw new IndexOutOfBoundsException(
                    "The number must be greater than or equal to zero.");
        }

        if (index >= size) {
            throw new IndexOutOfBoundsException(
                    "Index is larger than size of the list.");
        }

        if (size == 1) {
            return head;
        }

        Node<T> node;
        if (index < (size / 2)) {
            node = head;

            for (int i = 0; i < index; i++) {
                node = node.next;
            }
        } else {
            node = tail;

            for (int i = 0; i < (size - 1) - index; i++) {
                node = node.prev;
            }
        }

        return node;

    }

    /**
     * Удаление элемента по индексу.
     * При удалении происходит сдвиг элементов влево, начиная с index+1 и далее.
     *
     * @param index индекс
     */
    @Override
    public void remove(final int index) {
        removeElement(index);
        size--;
    }

    private void removeElement(final int index) {

        if (size == 0) {
            throw new IndexOutOfBoundsException("List is empty.");
        }

        if (index < 0) {
            throw new IndexOutOfBoundsException(
                    "The number must be greater than or equal to zero.");
        }

        if (index >= size) {
            throw new IndexOutOfBoundsException(
                    "Index is larger than size of the list.");
        }

        if (size == 1) {
            head = null;
            tail = null;
            return;
        }

        if (index == 0) {
            Node<T> next;
            next = head.next;
//            if (next != null) {
                next.prev = null;
//            }
            head = next;
            return;
        }

        if (index == (size - 1)) {
            Node<T> prev;
            prev = tail.prev;
            prev.next = null;
            tail = prev;
        } else {
            //  [i-1]  <prv[i]next>  [i+1]
            //  [i-1]next> [i+1]
            //  [i-1] <prev[i+1]
            Node<T> remove;
            remove = findNode(index);
            remove.prev.next = remove.next;
            remove.next.prev = remove.prev;
        }

    }

    /**
     * Получение индекса элемента по его значению.
     *
     * @param value элемент
     * @return индекс элемента или -1 если не найден
     */
    @Override
    public int indexOf(final T value) {
        Node<T> node = head;

        for (int i = 0; i < size; i++) {
            if (node.value.equals(value)) {
                return (i);
            }
            node = node.next;
        }
        return -1;
    }

    /**
     * Получение размера списка(количество элементов).
     *
     * @return размер списка
     */
    @Override
    public int size() {
        return size;
    }

    /**
     * Transform collection to String.
     * @return String
     */
    @Override
    public String toString() {

        if (size == 0) {
            return "[]";
        }

        StringBuilder temp = new StringBuilder("[");
        Node<T> element = head;
        for (int i = 0; i < size; i++) {
            temp.append(element.value);
            if (i < (size - 1)) {
                temp.append(", ");
            }
            element = element.next;
        }
        temp.append("]");

        return temp.toString();
    }
}
