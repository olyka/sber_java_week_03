package ru.edu;

/**
 * Структуры данных "очередь" на двусвязного списка.
 * @param <T>
 */
public class LinkedSimpleQueue<T> implements SimpleQueue {

    /**
     * List for storing queue.
     */
    private final LinkedSimpleList<T> list = new LinkedSimpleList<>();

    /**
     * Добавление элемента в конец очереди.
     *
     * @param value элемент
     * @return true - если удалось поместить элемент (если есть место в очереди)
     */
    @Override
    public boolean offer(final Object value) {

        if (value == null) {
            throw new NullPointerException("Null value cannot be queued.");
        }

        list.add((T) value);
        return true;

    }

    /**
     * Получение и удаление первого элемента из очереди.
     *
     * @return первый элемент из очереди
     */
    @Override
    public Object poll() {

        T element = list.get(0);
        list.remove(0);
        return element;

    }

    /**
     * Получение БЕЗ удаления первого элемента из очереди.
     *
     * @return первый элемент из очереди
     */
    @Override
    public Object peek() {
        return list.get(0);
    }

    /**
     * Количество элементов в очереди.
     *
     * @return количество элементов
     */
    @Override
    public int size() {
        return list.size();
    }

    /**
     * Количество элементов которое может уместиться в очереди.
     *
     * @return -1 если не ограничено (будет расширяться),
     * либо конкретное число если ограничено.
     */
    @Override
    public int capacity() {
        return -1;
    }

    /**
     * Transform collection to String.
     * @return String
     */
    @Override
    public String toString() {
        return list.toString();
    }
}
