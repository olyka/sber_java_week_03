package ru.edu;

import java.util.Arrays;

/**
 * Cтруктуры данных "список" на основе массива.
 * @param <T>
 */
public class ArraySimpleList<T> implements SimpleList<T> {

    /**
     * Array for storing data.
     */
    private T[] arr;

    /**
     * Current size of the list.
     */
    private int size;

    /**
     * Class constructor builds array of object with knows capacity.
     * @param capacity
     */
    public ArraySimpleList(final int capacity) {
        this.arr = (T[]) new Object[capacity];
    }


    /**
     * Добавление элемента в конец списка.
     *
     * @param value элемент
     */
    @Override
    public void add(final T value) {

//        if ((size + 1) == arr.length) {
//            arr = Arrays.copyOf(arr, arr.length * 2);
//        }

        int index;
        index = size;

        if (size == arr.length) {
            arr = Arrays.copyOf(arr, arr.length * 2);
        }

        arr[index] = value;
        size++;

    }

    /**
     * Установка значения элемента по индексу.
     *
     * @param index индекс
     * @param value элемент
     */
    @Override
    public void set(final int index, final T value) {

        if (size == 0) {
            throw new IndexOutOfBoundsException("List is empty.");
        }

        if (index < 0) {
            throw new IndexOutOfBoundsException(
                    "The number must be greater than or equal to zero.");
        }

        if (index >= size) {
            throw new IndexOutOfBoundsException(
                    "Index is larger than size of the list.");
        }

        arr[index] = value;

    }

    /**
     * Получение элемента из списка.
     *
     * @param index индекс
     * @return значение элемента или null
     */
    @Override
    public T get(final int index) {

        if (size == 0) {
            throw new IndexOutOfBoundsException("List is empty.");
        }

        if (index < 0) {
            throw new IndexOutOfBoundsException(
                    "The number must be greater than or equal to zero.");
        }

        if (index >= size) {
            throw new IndexOutOfBoundsException(
                    "Index is larger than size of the list.");
        }

        return arr[index];
    }

    /**
     * Удаление элемента по индексу.
     * При удалении происходит сдвиг элементов влево, начиная с index+1 и далее.
     *
     * @param index индекс
     */
    @Override
    public void remove(final int index) {

        if (size == 0) {
            throw new IndexOutOfBoundsException("List is empty.");
        }

        if (index < 0) {
            throw new IndexOutOfBoundsException(
                    "The number must be greater than or equal to zero.");
        }

        if (index >= size) {
            throw new IndexOutOfBoundsException(
                    "Index is larger than size of the list.");
        }

        for (int i = (index + 1); i < size; i++) {
            arr[i - 1] = arr[i];
        }

        size--;
        arr[size] = null;

    }

    /**
     * Получение индекса элемента по его значению.
     *
     * @param value элемент
     * @return индекс элемента или -1 если не найден
     */
    @Override
    public int indexOf(final T value) {

        for (int i = 0; i < size; i++) {
            if ((arr[i].equals(value))) {
                return i;
            }
        }

        return -1;

    }

    /**
     * Получение размера списка(количество элементов).
     *
     * @return размер списка
     */
    @Override
    public int size() {
        return size;
    }

    /**
     * Transform collection to String.
     * @return String
     */
    @Override
    public String toString() {

        if (size == 0) {
            return "[]";
        }

        StringBuilder temp = new StringBuilder("[");
        for (int i = 0; i < size; i++) {
            temp.append(arr[i]);
            if (i < (size - 1)) {
                temp.append(", ");
            }
        }
        temp.append("]");

        return temp.toString();
    }

}
