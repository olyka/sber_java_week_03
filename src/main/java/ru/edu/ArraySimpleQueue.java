package ru.edu;

/**
 * Структуры данных "очередь" на основе массива.
 * @param <T>
 */
public class ArraySimpleQueue<T> implements SimpleQueue  {

    /**
     * Array for storing queue.
     */
    private ArraySimpleList<T> list;

    /**
     * Capacity of the queue.
     */
    private int capacity;

    /**
     * Class constructor builds array of object with knows capacity.
     * @param cap
     */
    public ArraySimpleQueue(final int cap) {
        this.capacity = cap;
        this.list = new ArraySimpleList<>(cap);
    }

    /**
     * Добавление элемента в конец очереди.
     *
     * @param value элемент
     * @return true - если удалось поместить элемент (если есть место в очереди)
     */
    @Override
    public boolean offer(final Object value) {

        if (value == null) {
            throw new NullPointerException("Null value cannot be queued.");
        }

        if ((list.size() + 1) > capacity) {
            return false;
        }

        list.add((T) value);
        return true;
    }

    /**
     * Получение и удаление первого элемента из очереди.
     *
     * @return первый элемент из очереди
     */
    @Override
    public Object poll() {

        T element = list.get(0);
        list.remove(0);
        return element;

    }

    /**
     * Получение БЕЗ удаления первого элемента из очереди.
     *
     * @return первый элемент из очереди
     */
    @Override
    public Object peek() {
        return list.get(0);
    }

    /**
     * Количество элементов в очереди.
     *
     * @return количество элементов
     */
    @Override
    public int size() {
        return list.size();
    }

    /**
     * Количество элементов которое может уместиться в очереди.
     *
     * @return -1 если не ограничено (будет расширяться),
     * либо конкретное число если ограничено.
     */
    @Override
    public int capacity() {
        return capacity;
    }

    /**
     * Transform collection to String.
     * @return String
     */
    @Override
    public String toString() {
        return list.toString();
    }
}
