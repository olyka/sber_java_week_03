package ru.edu;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ArraySimpleListTest {

    private SimpleList<String> list;
    final static String FIRST = "First";
    final static String SECOND = "Second";

    @Before
    public void setUp() throws Exception {
        list = new ArraySimpleList<>(10);
    }

    @Test
    public void addTest() {
        assertEquals(0, list.size());
        list.add(FIRST);
        assertEquals(1, list.size());
        list.add(SECOND);
        assertEquals(2, list.size());

        assertEquals(list.toString(), "[" + FIRST + ", " + SECOND + "]");
        System.out.println(list);
    }

    @Test
    public void addMoreTest() {
        for (int i = 0; i < 10; i++) {
            list.add(String.valueOf(i + 1));
        }

        assertEquals(10, list.size());
        list.add(FIRST);
        assertEquals(11, list.size());
        list.add(SECOND);
        assertEquals(12, list.size());

        assertEquals(list.toString(), "[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, First, Second]");
        System.out.println(list);
    }

    @Test (expected = IndexOutOfBoundsException.class)
    public void getElementFromEmptyList() {
        list.get(5);
    }

    @Test (expected = IndexOutOfBoundsException.class)
    public void getElementWithWrongIndex() {
        for (int i = 0; i < 5; i++) {
            list.add(String.valueOf(i + 1));
        }
        list.get(20);
    }

    @Test (expected = IndexOutOfBoundsException.class)
    public void getElementWithNegativeIndex() {
        for (int i = 0; i < 5; i++) {
            list.add(String.valueOf(i + 1));
        }
        list.get(-20);
    }

    @Test
    public void getTest() {

        for (int i = 0; i < 10; i++) {
            list.add(String.valueOf(i + 1));
        }
        list.add(null);
        list.add(FIRST);
        list.add(SECOND);
        System.out.println(list);

        assertEquals(13, list.size());
        assertEquals("1", list.get(0));
        assertEquals("4", list.get(3));
        assertEquals("7", list.get(6));
        assertEquals("10", list.get(9));
        assertNull(list.get(10));
        assertEquals(SECOND, list.get(12));

    }

    @Test (expected = IndexOutOfBoundsException.class)
    public void setElementFromEmptyList() {
        list.set(5, FIRST);
    }

    @Test (expected = IndexOutOfBoundsException.class)
    public void setElementWithWrongIndex() {
        for (int i = 0; i < 5; i++) {
            list.add(String.valueOf(i + 1));
        }
        list.set(20, SECOND);
    }

    @Test (expected = IndexOutOfBoundsException.class)
    public void setElementWithNegativeIndex() {
        for (int i = 0; i < 5; i++) {
            list.add(String.valueOf(i + 1));
        }
        list.set(-20, SECOND);
    }

    @Test
    public void setTest() {

        for (int i = 0; i < 10; i++) {
            list.add(String.valueOf(i + 1));
        }

        list.set(0, FIRST);
        assertEquals(FIRST, list.get(0));
        list.set(3, SECOND);
        assertEquals(SECOND, list.get(3));
        list.set(6, FIRST + SECOND);
        assertEquals(FIRST + SECOND, list.get(6));
        list.set(9, SECOND + FIRST);
        assertEquals(SECOND + FIRST, list.get(9));

        assertEquals(10, list.size());
        System.out.println(list);

    }


    @Test (expected = IndexOutOfBoundsException.class)
    public void removeElementFromEmptyList() {
        list.remove(5);
    }

    @Test (expected = IndexOutOfBoundsException.class)
    public void removeElementWithWrongIndex() {
        for (int i = 0; i < 5; i++) {
            list.add(String.valueOf(i + 1));
        }
        list.remove(20);
    }

    @Test (expected = IndexOutOfBoundsException.class)
    public void removeElementWithNegativeIndex() {
        for (int i = 0; i < 5; i++) {
            list.add(String.valueOf(i + 1));
        }
        list.remove(-20);
    }

    @Test
    public void remove() {
        for (int i = 0; i < 10; i++) {
            list.add(String.valueOf(i + 1));
        }

        list.remove(0);
        assertEquals(9, list.size());
        assertEquals(list.toString(), "[2, 3, 4, 5, 6, 7, 8, 9, 10]");
        System.out.println(list);

        list.remove(8);
        assertEquals(8, list.size());
        assertEquals(list.toString(), "[2, 3, 4, 5, 6, 7, 8, 9]");
        System.out.println(list);

        list.remove(6);
        assertEquals(7, list.size());
        assertEquals(list.toString(), "[2, 3, 4, 5, 6, 7, 9]");
        System.out.println(list);

    }

    @Test
    public void indexOf() {

        for (int i = 0; i < 10; i++) {
            list.add(String.valueOf(i + 1));
        }
        list.add("10");
        list.add("10");
        list.add("10");

        assertEquals(13, list.size());

        assertEquals(0, list.indexOf("1"));
        assertEquals(3, list.indexOf("4"));
        assertEquals(6, list.indexOf("7"));
        assertEquals(9, list.indexOf("10"));
        assertEquals(-1, list.indexOf(null));
        assertEquals(-1, list.indexOf("12345"));

    }
}