package ru.edu;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ArraySimpleQueueTest {

    private ArraySimpleQueue<String> list;
    final static String FIRST = "First";
    final static String SECOND = "Second";
    private static final int CAPACITY = 5;

    @Before
    public void setUp() {
        list = new ArraySimpleQueue<>(CAPACITY);
    }

    @Test (expected = NullPointerException.class)
    public void offerNull() {
        list.offer(null);
    }

    @Test
    public void offer() {
        assertEquals(0, list.size());
        assertTrue(list.offer(FIRST));
        assertEquals(1, list.size());
        assertTrue(list.offer(SECOND));
        assertEquals(2, list.size());
        assertTrue(list.offer(FIRST));
        assertEquals(3, list.size());
        assertTrue(list.offer(SECOND));
        assertEquals(4, list.size());
        assertTrue(list.offer(FIRST));
        assertEquals(5, list.size());
        assertFalse(list.offer(SECOND));

        assertEquals(list.toString(), "["
                + FIRST + ", " + SECOND + ", "
                + FIRST + ", " + SECOND + ", " + FIRST
                + "]");
        System.out.println(list);
    }

    @Test (expected = IndexOutOfBoundsException.class)
    public void pollElementFromEmptyList() {
        list.poll();
    }

    @Test
    public void poll() {

        for (int i = 0; i < CAPACITY; i++) {
            list.offer(String.valueOf(i + 1));
        }

        assertEquals("1", list.poll());
        System.out.println(list);

    }

    @Test (expected = IndexOutOfBoundsException.class)
    public void peekElementFromEmptyList() {
        list.peek();
    }

    @Test
    public void peek() {

        for (int i = 0; i < CAPACITY; i++) {
            list.offer(String.valueOf(i + 1));
        }

        assertEquals("1", list.peek());
        System.out.println(list);
    }

    @Test
    public void capacity() {

        assertEquals(CAPACITY, list.capacity());
        System.out.println(list.capacity());

    }

    @Test
    public void size() {

        for (int i = 0; i < (CAPACITY * 2); i++) {
            list.offer(String.valueOf(i + 1));
        }

        assertEquals(CAPACITY, list.capacity());
        assertEquals(CAPACITY, list.size());
        System.out.println("capacity = " + list.capacity() + ", size = " + list.size());

    }

}