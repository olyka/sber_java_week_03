package ru.edu;

import org.junit.Before;
import org.junit.Test;

import java.util.Optional;

import static org.junit.Assert.*;

public class LinkedSimpleQueueTest {

    private LinkedSimpleQueue<String> list = new LinkedSimpleQueue<>();
    final static String FIRST = "First";
    final static String SECOND = "Second";
    final static int LIST_LENGTH = 5;

    @Test (expected = NullPointerException.class)
    public void offerNull() {
        list.offer(null);
    }

    @Test
    public void offer() {

        assertEquals(0, list.size());
        assertTrue(list.offer(FIRST));
        assertEquals(1, list.size());
        assertTrue(list.offer(SECOND));
        assertEquals(2, list.size());
        assertTrue(list.offer(FIRST));
        assertEquals(3, list.size());

        assertEquals("["
                + FIRST + ", " + SECOND + ", " + FIRST
                + "]", list.toString());
        System.out.println(list);

    }

    @Test (expected = IndexOutOfBoundsException.class)
    public void pollElementFromEmptyList() {
        list.poll();
    }

    @Test
    public void poll() {

        createList();
        assertEquals("1", list.poll());
        assertEquals("[2, 3, 4, 5]", list.toString());
        System.out.println(list);

    }

    @Test (expected = IndexOutOfBoundsException.class)
    public void peekElementFromEmptyList() {
        list.peek();
    }

    @Test
    public void peek() {

        createList();
        assertEquals("1", list.peek());
        System.out.println(list);

    }

    @Test
    public void capacity() {
        assertEquals(-1, list.capacity());
    }


    @Test
    public void size() {

        createList();
        assertEquals(5, list.size());

    }

    @Test
    public void testToString() {

        createList();
        assertEquals("[1, 2, 3, 4, 5]", list.toString());

    }

    private void createList() {
        for (int i = 0; i < LIST_LENGTH; i++) {
            list.offer(String.valueOf(i + 1));
        }
    }

}